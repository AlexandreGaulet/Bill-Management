<?php

namespace App\Controller;

use App\Entity\Invoice;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class InvoiceIncrementationController {

    public function __construct(private ObjectManager $manager){
    }

    public function __invoke(Invoice $data )
    {
        $data->setChrono($data->getChrono() + 1);

        $this->manager->flush();

        return $data;
    }
}
