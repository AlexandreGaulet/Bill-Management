<?php

namespace App\Event;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\Invoice;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class InvoiceEvent implements EventSubscriberInterface {

    public function __construct(private EntityManagerInterface $em){}

    public static function getSubscribedEvents():array {
        return [ KernelEvents::VIEW => ['chrono',  EventPriorities::PRE_VALIDATE] ];
    }

    public function chrono(ViewEvent $event) {
        $invoice = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ($invoice::class === Invoice::class && $method === 'POST') {
            $repository = $this->em->getRepository(Invoice::class);
            $qb = $repository->createQueryBuilder('f');
            $result = $qb->select('f.chrono')
               ->orderBy('f.id', 'DESC')
               ->setMaxResults(1)
               ->getQuery()
               ->getSingleScalarResult();

            $date = new \DateTime();
            $invoice->setChrono($result + 1)
                    ->setSentAt($date);
        }
    }
}