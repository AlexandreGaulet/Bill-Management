<?php

namespace App\Event;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\Customer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Bundle\SecurityBundle\Security;

final class CustomerUserSubscriber implements EventSubscriberInterface {

    public function __construct(private Security $security){}

    public static function getSubscribedEvents():array {
        return [ KernelEvents::VIEW => ['userSubscriber',  EventPriorities::PRE_VALIDATE] ];
    }

    public function userSubscriber(ViewEvent $event) {
        $customer = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ($customer::class === Customer::class && $method === 'POST') {
            $user = $this->security->getUser();
            $customer->setUser($user);
        }
         // dd($customer);
    }
}