<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(normalizationContext: ["groups" => ['read_customer']])]
#[ORM\Entity(repositoryClass: CustomerRepository::class)]
class Customer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read_customer', 'read_invoice', 'read_user'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read_customer', 'read_invoice', 'read_user'])]
    private ?string $firstName = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read_customer', 'read_invoice', 'read_user'])]
    private ?string $lastName = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read_customer', 'read_invoice', 'read_user'])]
    private ?string $email = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read_customer', 'read_invoice', 'read_user'])]
    private ?string $company = null;

    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: Invoice::class)]
    #[Groups(['read_customer', 'read_user'])]
    private Collection $invoices;

    #[Groups(['read_customer', 'read_invoice', 'read_user'])]
    #[ORM\ManyToOne(inversedBy: 'customers')]
    private ?User $user = null;

    public function __construct()
    {
        $this->invoices = new ArrayCollection();
    }

    #[Groups('read_customer', 'read_user')]
    public function getTotalInvoiceAmount() {
        return array_reduce($this->invoices->toArray(), function($total, $invoice) {
            if ($invoice->getStatus() !== 'CANCELLED') {
                return $total + $invoice->getAmount();
            }
            return $total;
        }, 0);
    }

    #[Groups(['read_customer', 'read_user'])]
    public function getTotalInvoicePaid() {
        return array_reduce($this->invoices->toArray(), function($total, $invoice) {
            if ($invoice->getStatus() === 'PAID') {
                return $total + $invoice->getAmount();
            }
            return $total;
        },0);
    }

    #[Groups(['read_customer', 'read_user'])]
    public function getTotalInvoiceUnpaid() {
        return array_reduce($this->invoices->toArray(), function($total, $invoice) {
            if ($invoice->getStatus() === 'SEND') {
                return $total + $invoice->getAmount();
            }
            return $total;
        },0);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection<int, Invoice>
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices->add($invoice);
            $invoice->setCustomer($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getCustomer() === $this) {
                $invoice->setCustomer(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
