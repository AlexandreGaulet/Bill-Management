<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\InvoiceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Link;
use App\Controller\InvoiceIncrementationController;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    uriTemplate: '/customers/{id}/invoices', // path name can change here
    uriVariables: [
        'id' => new Link(fromClass: Customer::class, fromProperty: 'invoices'),
    ],
    operations: [
        new GetCollection()
    ],
    // normalizationContext: ['groups' => ['read_invoice']] // can be use for have customer and change nameGroup
)]
// paginationClientItemsPerPage: true | items per page can be change by clients in query parameter: ?itemsPerPage=5
// paginationEnabled: true | pagination activate
// #[ApiResource(paginationEnabled: true, paginationClientItemsPerPage: true)]
#[ApiResource(normalizationContext: ['groups' => ['read_invoice']], operations: [
    new Get(uriTemplate: '/invoices/{id}'),
    new Post(),
    new GetCollection(),
    new Post(uriTemplate: '/invoices/{id}/incrementation', controller: InvoiceIncrementationController::class, description: 'incrementation invoice')
])]

#[ApiFilter(OrderFilter::class, properties: ['sentAt' => 'ASC'])]
#[ApiFilter(SearchFilter::class, properties: ['chrono', 'status'])] // 
#[ApiFilter(DateFilter::class, properties: ['sentAt' => DateFilter::INCLUDE_NULL_BEFORE_AND_AFTER])]
#[ORM\Entity(repositoryClass: InvoiceRepository::class)]
class Invoice
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read_invoice', 'read_customer'])]
    private ?int $id = null;

    #[ORM\Column]
    #[Groups(['read_invoice', 'read_customer'])]
    private ?float $amount = null;

    #[ORM\Column]
    #[Groups(['read_invoice', 'read_customer'])]
    private ?\DateTime $sentAt = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read_invoice', 'read_customer'])]
    #[Assert\Choice(choices: ['SEND', 'PAID', 'CANCELLED'], message: 'Le status dois etre SEND, PAID ou CANCELLED')]
    private ?string $status = null;

    #[ORM\ManyToOne(inversedBy: 'invoices')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['read_invoice'])]
    private ?Customer $customer = null;

    #[ORM\Column]
    #[Groups(['read_invoice', 'read_customer'])]
    private ?int $chrono = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getSentAt(): ?\DateTime
    {
        return $this->sentAt;
    }

    public function setSentAt(\DateTime $sentAt): self
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getChrono(): ?int
    {
        return $this->chrono;
    }

    public function setChrono(int $chrono): self
    {
        $this->chrono = $chrono;

        return $this;
    }
}
