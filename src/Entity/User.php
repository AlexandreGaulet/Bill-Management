<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use ApiPlatform\Metadata\ApiResource;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(normalizationContext: ["groups" => ['read_user']])]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity('email',  message: 'Ce mail est déjà utilisée.')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read_user'])]
    private ?int $id = null;

    #[Groups(['read_user', 'read_invoice', 'read_customer'])]
    #[ORM\Column(length: 180, unique: true)]
    #[Assert\Email(
        message: 'The email {{ value }} is not a valid email.',
    )]
    #[Assert\NotBlank(
        message: 'Vous devais rensegner le champs Email',
    )]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    #[Assert\NotBlank(
        message: 'Vous devais rensegner le champs Mot de passe',
    )]
    #[Assert\Length(
        min: 6,
        max: 20,
        minMessage: 'Votre Mot de passe doit comporter au moins {{ limit }} caractères.',
        maxMessage: 'Votre Mot de passe ne peut pas comporter plus de {{ limit }} caractères.',
    )]
    #[Assert\Regex(
        pattern: '/\d+/',
        match: true,
        message: 'il dois y avoir au moins un chiffre',
    )]
    #[Assert\Regex(
        pattern: '/[a-zA-Z]+/',
        match: true,
        message: 'il dois y avoir au moins une lettre',
    )]
    #[Assert\Regex(
        pattern: '/[\W_]+/',
        match: true,
        message: 'il dois y avoir au moins un caractere speciaux',
    )]
    private ?string $password = null;

    #[Groups(['read_user', 'read_invoice', 'read_customer'])]
    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(
        message: 'Vous devais rensegner le champs Prenom',
    )]
    private ?string $firstName = null;

    #[Groups(['read_user', 'read_invoice', 'read_customer'])]
    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(
        message: 'Vous devais rensegner le champs Nom de famille',
    )]
    private ?string $lastName = null;

    #[Groups(['read_user'])]
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Customer::class)]
    private Collection $customers;

    public function __construct()
    {
        $this->customers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return Collection<int, Customer>
     */
    public function getCustomers(): Collection
    {
        return $this->customers;
    }

    public function addCustomer(Customer $customer): self
    {
        if (!$this->customers->contains($customer)) {
            $this->customers->add($customer);
            $customer->setUser($this);
        }

        return $this;
    }

    public function removeCustomer(Customer $customer): self
    {
        if ($this->customers->removeElement($customer)) {
            // set the owning side to null (unless already changed)
            if ($customer->getUser() === $this) {
                $customer->setUser(null);
            }
        }

        return $this;
    }
}
